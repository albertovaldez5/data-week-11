- [Introduction](#introduction)
- [Running Javascript](#running-javascript)
  - [Running from HTML](#running-from-html)
  - [Running from a file](#running-from-a-file)
- [Syntax](#syntax)
  - [Defining Variables](#defining-variables)
  - [Operations](#operations)
  - [Strings](#strings)
  - [Templates](#templates)
- [Control Flow](#control-flow)
  - [If Statements](#if-statements)
  - [Comparison Operators](#comparison-operators)
- [Data Structures](#data-structures)
  - [Arrays](#arrays)
  - [Objects](#objects)
  - [Example: Array](#example-array)
- [Loops](#loops)
  - [Example: Loops](#example-loops)
- [Functions](#functions)
  - [Built-in Functions](#built-in-functions)
  - [Example: Functions](#example-functions)
- [Functional Javascript](#functional-javascript)
  - [For Each](#for-each)
  - [The Arrow Function](#the-arrow-function)
- [JSON](#json)
  - [JSON and Functions](#json-and-functions)
  - [Example: Objects](#example-objects)
- [CSS](#css)
  - [Bootstrap](#bootstrap)
- [D3](#d3)
  - [Example: D3](#example-d3)
  - [Python Test](#python-test)
- [Events](#events)
  - [D3 Event Listeners](#d3-event-listeners)
  - [Adding HTML](#adding-html)
  - [Example: Events](#example-events)



<a id="introduction"></a>

# Introduction

Javascript is the scripting language of the web, it is often minimized when running in the browser, so the variables may have reduced names and the formatting may be ugly. `ES6` is the latest standard for the language, most of the HTML nowadays is written by javascript frameworks or libraries, never directly by hand.

Javascript allows us to display data visualization in a very compelling and accesible way. The browser deals with all the connections between HTML and Javascript. The later was designed to be part of the web infrastructure, so we mostly need to deal with the logic of our scripts.

Mozilla has very complete documentation for Javascript. <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>


<a id="running-javascript"></a>

# Running Javascript

Since JS is executed in the browser, JS has no access to local files from the user, so it normally makes API calls using actions like `AJAX` to get its information. The most common use case for JS is react to user interaction.

Javascript can run:

1.  Directly from HTML.
2.  From a `.js` file referenced in the HTML.
3.  From a web browser&rsquo;s console.


<a id="running-from-html"></a>

## Running from HTML

```html
<script type="text/javascript">
  console.log("hello world");
</script>
```


<a id="running-from-a-file"></a>

## Running from a file

Assumming this is a file named `hello_world.js`.

```typescript
console.log("Hello world!");
```

    Hello world!

We can call it from the HTML with a `script` tag.

```html
<script src="../resources/hello_world.js"></script>
```

**NOTE**: Any script called from HTML like this will execute on start.


<a id="syntax"></a>

# Syntax


<a id="defining-variables"></a>

## Defining Variables

`let` creates a variable and gives it a value in that moment.

`var` creates a variable, regardless of value or not, you can assign it a value later.

`const` creates a constant, so the value can not be reassign.

```typescript
var fullName = "Homer Simpson";
let country = "United States";
const age = 42;
```

There are no data types in Javascript.

**NOTE**: The code blocks in this document are in Typescript because it&rsquo;s useful to use in this context. Typescript compiles to Javascript and it supports data types but we are not using them in this document. Any code from the Typescript source blocks here will run in Javascript without issues so it can be copied.


<a id="operations"></a>

## Operations

The following code is broken, even if it works, the data conversion is left to the browser.

```typescript
var hourlyWage = 15;
var weeklyHours = "40";
var weeklyWage = hourlyWage * weeklyHours;
console.log(weeklyWage);
```

    600

The Internal Type Conversion will try to guess the data type of whatever operation you are doing. Use `Number` instead, which will convert the value explicitly.

```typescript
var weeklyWage = 15 * Number("40");
console.log(weeklyWage);
```

    600


<a id="strings"></a>

## Strings

For concatenating strings, we can use the `+` operator.

```typescript
var wage = 600;
console.log("Wage is " + 600 + " a week.");
```

    Wage is 600 a week.


<a id="templates"></a>

## Templates

Templates start and end with the `` ` `` character.

```typescript
var country = "Mexico";
var year = 2021;
var offset = 1;
console.log(`You live in ${country} during the year ${year + offset}.`);
```

    You live in Mexico during the year 2022.


<a id="control-flow"></a>

# Control Flow


<a id="if-statements"></a>

## If Statements

Better to use triple equal sign `===` instead of any other type of comparison. Using double equal sign `==` will try to convert the data types, but using the triple equal sign will prevent that from happening. <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>

In this example we compare using a simple conditional v.s. a triple equal sign one.

```typescript
function is_satisfied(satisfied){
    if (satisfied){
        console.log("Satisfied: " + satisfied);
    }
    else {
        console.log("Not satisfied: " + satisfied);
    }
}
function is_satisfied2(satisfied){
    if (satisfied === true){
        console.log("Satisfied: " + satisfied);
    }
    else {
        console.log("Not satisfied: " + satisfied);
    }
}
console.log("No =");
is_satisfied(true);
is_satisfied([]);
is_satisfied("");
console.log("\nUsing ===");
is_satisfied2(true);
is_satisfied2([]);
is_satisfied2("");
```

    No =
    Satisfied: true
    Satisfied:
    Not satisfied:
    
    Using ===
    Satisfied: true
    Not satisfied:
    Not satisfied:


<a id="comparison-operators"></a>

## Comparison Operators

```typescript
// Not equal
if (0 != 1){}
// Less than
if (0 < 1){}
// Greater than
if (0 > 1){}
// Less or equal
if (0 <= 1){}
// And
if ( 0 === 1 && 1 === 1){}
// Org
if ( 0 === 1 || 1 === 1){}
// Nested
if (0 < 10){
    if (5 < 10){}
    else if (10 < 10){}
    else {}
}
```


<a id="data-structures"></a>

# Data Structures


<a id="arrays"></a>

## Arrays

```typescript
let inCalifornia = true;
let cityArray = ["San Francisco", 94123, [37.77, -122.43], inCalifornia];
let cityName = cityArray[0];
console.log(cityName);
```

    San Francisco


<a id="objects"></a>

## Objects

They are key, value pair structures but they are not dictionaries.

We can access the object with &ldquo;dictionary-like&rdquo; notation or with the &ldquo;dot&rdquo; notation.

Example: `object["valueName"]` or `object.valueName`.

```typescript
let cityObject = {
    city: "San Francisco",
    zip: 94123,
    location: [37.77, -122.43],
    inCalifornia: true
};
console.log(cityObject);
console.log("Zip: " + cityObject["zip"]);
console.log("Location: " + cityObject.location);
```

    {
      city: 'San Francisco',
      zip: 94123,
      location: [ 37.77, -122.43 ],
      inCalifornia: true
    }
    Zip: 94123
    Location: 37.77,-122.43

**NOTE**: We can store functions in variables, so objects can have methods by having functions as attributes.


<a id="example-array"></a>

## Example: Array

We will start with a simple array. Then we can index it with integers. Then add elements at the begining and end of the array.

```typescript
var letterArray = ["A", "B", "C", "D"];
console.log("--Array--\n" + letterArray);
let first = letterArray[0];
let second = letterArray[1];
console.log("Index 0 and 1", first, second);
letterArray.push("E");
letterArray.unshift("Z");
console.log("--Push and Unshift--\n" + letterArray);
var slice1 = letterArray.slice(4);
// Return the first three items of an array
console.log("--Sliced--\n" +  slice1);
// Return the second and third items of an array
var slice2 = letterArray.slice(1, 3);
console.log("--Sliced Range---\n", slice2);
// Use join() to return items of an array into a single string
var str =  letterArray.join(", ")
console.log("--Joined string--\n", str);
// A JavaScript string
var string1 = "Access element of string"
// Use indexing to access a string character
console.log(`--${string1}--\n`, string1[0], string1[1], string1[2], string1[3]);
```

    --Array--
    A,B,C,D
    Index 0 and 1 A B
    --Push and Unshift--
    Z,A,B,C,D,E
    --Sliced--
    D,E
    --Sliced Range---
     [ 'A', 'B' ]
    --Joined string--
     Z, A, B, C, D, E
    --Access element of string--
     A c c e


<a id="loops"></a>

# Loops

Loops just as with any other language. Bonus: checking for undefined.

```typescript
var students = ["Bob", "Alice", "Garfield", "Stitch"]
for (var i = 0; i < 5; i++){
    if (students[i] != undefined){
        console.log(i + "-" + students[i]);
    }
}
```

    0-Bob
    1-Alice
    2-Garfield
    3-Stitch


<a id="example-loops"></a>

## Example: Loops

In this example we will loop over an array and check if the value passes a condition, then add it to a new array depending on it.

```typescript
// Array of movie ratings
var movieScores = [
  4.4,
  3.3,
  5.9,
  8.8,
  1.2,
  5.2,
  7.4,
  7.5,
  7.2,
  9.7,
  4.2,
  6.9
];

// Starting a rating count
var sum = 0;

// Arrays to hold movie scores
var goodMovieScores = [];
var okMovieScores = [];
var badMovieScores = [];

for (var i = 0; i < movieScores.length; i++){
    let score = movieScores[i];
    sum += score;
    if (score >= 7){
        goodMovieScores.push(score);
    }
    else if (score >= 5 && score < 7){
        okMovieScores.push(score);
    }
    else {
        badMovieScores.push(score);
    }
}
var avg = sum / movieScores.length;
var nGood = goodMovieScores.length;
var nOk = okMovieScores.length;
var nBad = badMovieScores.length;
console.log("Average: " + avg);
console.log("Good movies count " + nGood);
console.log("Ok movies count " + nOk);
console.log("Bad movies count " + nBad);
```

    Average: 5.975000000000001
    Good movies count 5
    Ok movies count 3
    Bad movies count 4


<a id="functions"></a>

# Functions

We can create functions with the `function` keyword.

```typescript
function printHello(){
    console.log("Hello there!");
}
function addition(a, b){
    return a + b;
}
function listLoop(userList){
    for (let i = 0; i < userList.length; i++){
        console.log(i);
    }
}
// Calling functions.
printHello();
// Calling functions from functions.
function doubleAddition(a, b){
    // Using the result of a function.
    var res = addition(a, b) * 2;
    return res;
}
console.log(doubleAddition(2, 3));
```

    Hello there!
    10


<a id="built-in-functions"></a>

## Built-in Functions

We can use built-in functions from JS <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>

```typescript
var longDecimal = 112.3453410
var rounded = Math.floor(longDecimal);
console.log(rounded);
```

    112


<a id="example-functions"></a>

## Example: Functions

We will compute the standard deviation of an array by implementing our own functions.

```typescript
var movieScore = [4.4, 3.3, 5.9, 8.8, 1.2, 5.2, 7.4, 7.5, 7.2, 9.7, 4.2, 6.9];

function getMean(array){
    var total = 0;
    for (let i = 0; i < array.length; i++){
        total += array[i];
    }
    var meanValue = total / array.length;
    return meanValue;
}
function getVariance(array){
    var meanValue = getMean(array);
    var total = 0;
    for (var i = 0; i < array.length; i++){
        total += (array[i] - meanValue) ** 2;
    }
    var varianceValue = total / array.length;
    return varianceValue;
}
function getSTD(array){
    var varianceValue = getVariance(array);
    var STD = Math.sqrt(varianceValue);
    return STD;
}

var std = getSTD(movieScore);
console.log("Standard Deviation is: " + std);
```

    Standard Deviation is: 2.3231175174751706

Because Javascript doesn&rsquo;t include a function for `mean` or `std`, we have to implement them ourselves. However we can always import math libraries to help us deal with it. <sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup>


<a id="functional-javascript"></a>

# Functional Javascript


<a id="for-each"></a>

## For Each

`forEach` requires a data structure that we want to work with and iterates over the elements within the structure.

For each loops come from the functional programming paradigm, where we mostly work with passing functions as arguments.

Each object in the data structure will have the given function applied to it. Also known as executing the callback in each of the elements.

```typescript
var data = [1, 2, 4, 3]
var biggest = 0;
var sum = 0;

function cumulativesum(score){
  if (score > biggest){
    biggest = score;
  }
  sum += score;
}
// Using forEach
data.forEach(cumulativesum);
console.log("Cumulative Sum:", sum);
console.log("Biggest Number:", biggest);
```

    Cumulative Sum: 10
    Biggest Number: 4


<a id="the-arrow-function"></a>

## The Arrow Function

An arrow function is an anonymus function, the syntax comes from the `ES6` standard and it&rsquo;s the same as writing `function(args){}` but using the arrow for better readability `(args) => {}`.

```typescript
let data = [0, 1, 2, 3]
data.forEach((score) => {
  console.log(score);
})
```

    0
    1
    2
    3


<a id="json"></a>

# JSON

The Javascript Object Notation is a standard for storing data as a JS object.

We can access the members of a JSON with a key or dot notation. We can also use the `Object` global type to access data from the object.

```typescript
let people = {
  mom: "wilma",
  dad: "fred",
  daughter: "pebbles",
  son: "bambam"
};
let key = "mom";
console.log(people[key]);
console.log(people.son);
// Accessing properties
console.log(Object.keys(people));
console.log(Object.values(people));
console.log(Object.entries(people));
```

    wilma
    bambam
    [ 'mom', 'dad', 'daughter', 'son' ]
    [ 'wilma', 'fred', 'pebbles', 'bambam' ]
    [
      [ 'mom', 'wilma' ],
      [ 'dad', 'fred' ],
      [ 'daughter', 'pebbles' ],
      [ 'son', 'bambam' ]
    ]


<a id="json-and-functions"></a>

## JSON and Functions

We can combine JSON and For Each to execute an arrow function in each element of the object.

```typescript
let character = {
  name: "homer",
  age: 42,
  simpson: true
}
Object.entries(character).forEach(([key, value]) => {
  console.log(`Key: ${key}, Value: ${value}`)
});
```

    Key: name, Value: homer
    Key: age, Value: 42
    Key: simpson, Value: true

We used the builtin `Object` and its method `entries` to use the object as an array of key, value pairs.


<a id="example-objects"></a>

## Example: Objects

We can create a function that counts how many times a word appears in a string by applying all the previous concepts.

```typescript
function countWordFreq(text){
  var array = text.split(" ");
  var wordFreq = {};
  // Iterate through the array and count the occurrence of each word
  array.forEach((word) => {
    word = word.toLowerCase();
    if (word in wordFreq){
      wordFreq[word] += 1;
    }
    else {
      wordFreq[word] = 1;
    }
  });
  return wordFreq;
}
//  Call the function with the string as a parameter.
let text = "Red red green blue red bluE"
console.log(text);
console.log(countWordFreq(text));
text = "I yam what I yam and always will be what I yam"
console.log(text);
console.log(countWordFreq(text));
```

    Red red green blue red bluE
    { red: 3, green: 1, blue: 2 }
    I yam what I yam and always will be what I yam
    { i: 3, yam: 3, what: 2, and: 1, always: 1, will: 1, be: 1 }


<a id="css"></a>

# CSS

We can style our tables, divs and elements by using Cascading Style Sheets. CSS is applied in this order `Tag->Class->ID`.

```css
/* Tag */
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
/* Class */
.text-2u {
    color: hotpink;
}
/* ID */
#NY {
    font-size: 16px;
}
```

We can import stylesheets in the `<head>` part of the HTML document.

```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
```


<a id="bootstrap"></a>

## Bootstrap

We can include pre-made CSS styles by importing them in the header. We must use the classes provided by whichever pre-made style we choose.

We can always mix the Bootstrap CSS with our own. It is always a good idea to look at the documentation <sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup>.


<a id="d3"></a>

# D3

Data Driven Documents <sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup> is a Javascript library to visualize data. To use it we import it in our html header.

```html
<script src="https://d3js.org/d3.v5.min.js"></script>
```

D3 uses Scalable Vector Graphics <sup><a id="fnr.7" class="footref" href="#fn.7" role="doc-backlink">7</a></sup> to display the data with vectors instead of bitmaps.

We can include data from external files by adding a `<script>` tag in our HTML document. This will include the objects and functions from those scripts in our document.

```html
<head>
</head>
<body>
</body>
<script src="data.js"></script>
<script src="index_starter.js"></script>
```


<a id="example-d3"></a>

## Example: D3

This is our base HTML document.

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <script src="https://d3js.org/d3.v5.min.js"></script>
</head>

<body>
  <table>
    <thead>
      <tr>
        <th>Weekday</th>
        <th>Date</th>
        <th>High</th>
        <th>Low</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
</body>
<script src="data.js"></script>
<script src="index_starter.js"></script>

</html>
```

This is our `data.js`. We will access the data array from our other javascript file.

```typescript
const data = [
  {
    weekday: "SUN",
    date: "July 1",
    high: 76,
    low: 63
  }, {
    weekday: "MON",
    date: "July 2",
    high: 77,
    low: 63
  }, {
    weekday: "TUE",
    date: "July 3",
    high: 77,
    low: 63
  }, {
    weekday: "WED",
    date: "July 4",
    high: 81,
    low: 65
  }, {
    weekday: "THU",
    date: "July 5",
    high: 87,
    low: 68
  }, {
    weekday: "FRI",
    date: "July 6",
    high: 91,
    low: 71
  }, {
    weekday: "SAT",
    date: "July 7",
    high: 91,
    low: 72
  }
];
```

We will append `<tr>` nodes to our table body using a script. We don&rsquo;t have the nodes in our HTML file by default but Javascript will add the to the DOM (which is the result of the HTMl + JS).

```typescript
let tbody = d3.select("tbody");
// Console.log the weather data from data.js
console.log(data);
// Append new rows to the table
data.forEach((weatherReport) => {
  let row = tbody.append("tr");
  Object.values(weatherReport).forEach((value) => row.append("td").text(value));
});
```


<a id="python-test"></a>

## Python Test

The Javascript code ends there, but we are going to sneak in a Python test for scraping the data from the HTML document and display it here.

```python
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup
from splinter import Browser
from pathlib import Path


def init_browser(url: str):
    executable_path = ChromeDriverManager().install()
    b = Browser("chrome", executable_path=executable_path, headless=True)
    b.visit(url)
    return b

parent = Path.cwd().parent
url = parent / "resources/11-2-Student_Resources/05-Evr_D3_Table/index.html"
# Absolute path using file://
browser = init_browser(f"file://{url}")
soup = BeautifulSoup(browser.html, 'html.parser')
table = soup.find("table")
browser.quit()
print(f"{table}")
```

<table>
<thead>
<tr>
<th>Weekday</th>
<th>Date</th>
<th>High</th>
<th>Low</th>
</tr>
</thead>
<tbody><tr><td>SUN</td><td>July 1</td><td>76</td><td>63</td></tr><tr><td>MON</td><td>July 2</td><td>77</td><td>63</td></tr><tr><td>TUE</td><td>July 3</td><td>77</td><td>63</td></tr><tr><td>WED</td><td>July 4</td><td>81</td><td>65</td></tr><tr><td>THU</td><td>July 5</td><td>87</td><td>68</td></tr><tr><td>FRI</td><td>July 6</td><td>91</td><td>71</td></tr><tr><td>SAT</td><td>July 7</td><td>91</td><td>72</td></tr></tbody>
</table>


<a id="events"></a>

# Events

We use events to react to actions from the user. There are many events available in Javascript <sup><a id="fnr.8" class="footref" href="#fn.8" role="doc-backlink">8</a></sup>, here are some of the most common:

-   Click
-   Change
-   Keydown
-   Scroll
-   Pointerender


<a id="d3-event-listeners"></a>

## D3 Event Listeners

An event listener looks like this: `Object(Event, Handler);`.

```typescript
// Object( Event,  Handler    );
button.on("click", handleClick);
```

If we have a button in the html.

```html
<button id="click-me">Click me!</button>
<input id="input-field"></input>
```

We must create a reference to the button with D3.

```typescript
// Element
var button = d3.select("#click-me");
var inputField = d3.select("#input-field");
// Handler
function handleClick(){
  console.log("Button clicked!");
  console.log(d3.event.target);
}
// Object( Event,  Handler )
button.on("click", handleClick);
```

An event is an object that contains all the data from the interaction. The target property is a node in the DOM. So we can grab the target to get more information. This way we can know in which element the event happened.

In this case our target is the button with the id `"click-me"`.


<a id="adding-html"></a>

## Adding HTML

We can add HTML elements to the DOM dynamically using events.

```typescript
function handleClick(){
  let text = "Hello D3!";
  // Selecting a class and adding a header to it.
  d3.select(".giphy-me").html(`<h1>${text}</h1>`);
}
```


<a id="example-events"></a>

## Example: Events

We need to create a script that will react to user input and create a reversed string in a new HTML element.

```typescript
var output = d3.select(".output"); // class
var inputField = d3.select("#my-input"); // id
var ul = d3.select("#my-ul")
// Functions
function reverseString(str) {
  return str.split("").reverse().join("");
}
function handleChange(event) {
  // grab the value of the input field
  // Using global d3 event instead of local one from this function
  let value = d3.event.target.value;
  // clear the existing output
  output.html("");
  // reverse the input string
  let reversed = reverseString(value);
  // Set the output text to the reversed input string
  output.text(reversed);
}
// Event
inputField.on("input", handleChange);
```

We can load the script in this document as long as we are importing `D3`, it will work for the HTML version but not for Markdown.

<div id="my-div">
    <label id="my-label" for="text">Input Text: </label>
    <input id="my-input" type="text" name="text-input", value="tacocat">
</div>
<h1 class="output"></h1>
<ul id="my-ul"></ul>
<script src="../resources/11-2-Student_Resources/07-Stu_onChange/js/app_starter.js"></script>

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://developer.mozilla.org/en-US/docs/Web/JavaScript>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://blog.openreplay.com/javascript-type-conversions-explained>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects>

<sup><a id="fn.4" class="footnum" href="#fnr.4">4</a></sup> <https://mathjs.org/docs/reference/functions/mean.html>

<sup><a id="fn.5" class="footnum" href="#fnr.5">5</a></sup> <https://getbootstrap.com/docs/5.2/getting-started/introduction/>

<sup><a id="fn.6" class="footnum" href="#fnr.6">6</a></sup> <https://observablehq.com/@d3/gallery>

<sup><a id="fn.7" class="footnum" href="#fnr.7">7</a></sup> <https://en.wikipedia.org/wiki/Scalable_Vector_Graphics>

<sup><a id="fn.8" class="footnum" href="#fnr.8">8</a></sup> <https://developer.mozilla.org/en-US/docs/Web/Events>
