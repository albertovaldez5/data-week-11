// [[file:javascript.org::#json][JSON:1]]
let people = {
  mom: "wilma",
  dad: "fred",
  daughter: "pebbles",
  son: "bambam"
};
let key = "mom";
console.log(people[key]);
console.log(people.son);
// Accessing properties
console.log(Object.keys(people));
console.log(Object.values(people));
console.log(Object.entries(people));
// JSON:1 ends here
