#+title:     Javascript
#+subtitle:  The language of the web
#+author:    Alberto Valdez
#+SETUPFILE: ../config/org-theme-alt.config
#+SETUPFILE: ../config/org-header.config
#+PROPERTY: header-args:typescript :post ts-to-js(data=*this*)
#+HTML_HEAD: <script src="https://d3js.org/d3.v5.min.js"></script>

#+NAME: ts-to-js
#+begin_src js :var data="var x = 2; console.log(x + 1);" :results silent :exports none
eval?.(`"use strict"; ()=>{${data}}`)();
#+end_src

* Introduction
:PROPERTIES:
:CUSTOM_ID: introduction
:END:

Javascript is the scripting language of the web, it is often minimized when running in the browser, so the variables may have reduced names and the formatting may be ugly. =ES6= is the latest standard for the language, most of the HTML nowadays is written by javascript frameworks or libraries, never directly by hand.

Javascript allows us to display data visualization in a very compelling and accesible way. The browser deals with all the connections between HTML and Javascript. The later was designed to be part of the web infrastructure, so we mostly need to deal with the logic of our scripts.

Mozilla has very complete documentation for Javascript. [fn:1]

* Running Javascript
:PROPERTIES:
:CUSTOM_ID: running-javascript
:END:

Since JS is executed in the browser, JS has no access to local files from the user, so it normally makes API calls using actions like =AJAX= to get its information. The most common use case for JS is react to user interaction.

Javascript can run:

1. Directly from HTML.
2. From a =.js= file referenced in the HTML.
3. From a web browser's console.

** Running from HTML
:PROPERTIES:
:CUSTOM_ID: running-from-html
:END:

#+begin_src html
<script type="text/javascript">
  console.log("hello world");
</script>
#+end_src

** Running from a file
:PROPERTIES:
:CUSTOM_ID: running-from-a-file
:END:

Assumming this is a file named =hello_world.js=.

#+begin_src typescript
console.log("Hello world!");
#+end_src

#+RESULTS[c332ab437cd95c692c6f409e578bf4f43deec062]:
#+begin_example
Hello world!
#+end_example

We can call it from the HTML with a =script= tag.

#+begin_src html
<script src="../resources/hello_world.js"></script>
#+end_src

*NOTE*: Any script called from HTML like this will execute on start.

* Syntax
:PROPERTIES:
:CUSTOM_ID: syntax
:END:

** Defining Variables
:PROPERTIES:
:CUSTOM_ID: defining-variables
:END:

=let= creates a variable and gives it a value in that moment.

=var= creates a variable, regardless of value or not, you can assign it a value later.

=const= creates a constant, so the value can not be reassign.

#+begin_src typescript :eval no
var fullName = "Homer Simpson";
let country = "United States";
const age = 42;
#+end_src

There are no data types in Javascript.

*NOTE*: The code blocks in this document are in Typescript because it's useful to use in this context. Typescript compiles to Javascript and it supports data types but we are not using them in this document. Any code from the Typescript source blocks here will run in Javascript without issues so it can be copied.

** Operations
:PROPERTIES:
:CUSTOM_ID: operations
:END:

The following code is broken, even if it works, the data conversion is left to the browser.

#+begin_src typescript
var hourlyWage = 15;
var weeklyHours = "40";
var weeklyWage = hourlyWage * weeklyHours;
console.log(weeklyWage);
#+end_src

#+RESULTS[1ccefa8f36c1c80657a20df35f6c9a32591ca620]:
#+begin_example
600
#+end_example

The Internal Type Conversion will try to guess the data type of whatever operation you are doing. Use =Number= instead, which will convert the value explicitly.

#+begin_src typescript
var weeklyWage = 15 * Number("40");
console.log(weeklyWage);
#+end_src

#+RESULTS[6d0f6c7d31f4a91542bd496fa9b9df33cddcbc38]:
#+begin_example
600
#+end_example

** Strings
:PROPERTIES:
:CUSTOM_ID: strings
:END:

For concatenating strings, we can use the =+= operator.

#+begin_src typescript
var wage = 600;
console.log("Wage is " + 600 + " a week.");
#+end_src

#+RESULTS[5d00fab063f0f66d807292e47ccb6b6072ec36eb]:
#+begin_example
Wage is 600 a week.
#+end_example

** Templates
:PROPERTIES:
:CUSTOM_ID: templates
:END:

Templates start and end with the =`= character.

#+begin_src typescript
var country = "Mexico";
var year = 2021;
var offset = 1;
console.log(`You live in ${country} during the year ${year + offset}.`);
#+end_src

#+RESULTS[0ce7d96450ef6f8006cc5c6005a0f883441e5728]:
#+begin_example
You live in Mexico during the year 2022.
#+end_example

* Control Flow
:PROPERTIES:
:CUSTOM_ID: control-flow
:END:

** If Statements
:PROPERTIES:
:CUSTOM_ID: if-statements
:END:

Better to use triple equal sign ===== instead of any other type of comparison. Using double equal sign ==== will try to convert the data types, but using the triple equal sign will prevent that from happening. [fn:2]

In this example we compare using a simple conditional v.s. a triple equal sign one.

#+begin_src typescript
function is_satisfied(satisfied){
    if (satisfied){
        console.log("Satisfied: " + satisfied);
    }
    else {
        console.log("Not satisfied: " + satisfied);
    }
}
function is_satisfied2(satisfied){
    if (satisfied === true){
        console.log("Satisfied: " + satisfied);
    }
    else {
        console.log("Not satisfied: " + satisfied);
    }
}
console.log("No =");
is_satisfied(true);
is_satisfied([]);
is_satisfied("");
console.log("\nUsing ===");
is_satisfied2(true);
is_satisfied2([]);
is_satisfied2("");
#+end_src

#+RESULTS[fbc1683ffc42ab34206b86d4561bdadd790fc065]:
#+begin_example
No =
Satisfied: true
Satisfied:
Not satisfied:

Using ===
Satisfied: true
Not satisfied:
Not satisfied:
#+end_example

** Comparison Operators
:PROPERTIES:
:CUSTOM_ID: comparison-operators
:END:

#+begin_src typescript :eval no
// Not equal
if (0 != 1){}
// Less than
if (0 < 1){}
// Greater than
if (0 > 1){}
// Less or equal
if (0 <= 1){}
// And
if ( 0 === 1 && 1 === 1){}
// Org
if ( 0 === 1 || 1 === 1){}
// Nested
if (0 < 10){
    if (5 < 10){}
    else if (10 < 10){}
    else {}
}
#+end_src


* Data Structures
:PROPERTIES:
:CUSTOM_ID: data-structures
:END:

** Arrays
:PROPERTIES:
:CUSTOM_ID: arrays
:END:

#+begin_src typescript
let inCalifornia = true;
let cityArray = ["San Francisco", 94123, [37.77, -122.43], inCalifornia];
let cityName = cityArray[0];
console.log(cityName);
#+end_src

#+RESULTS[e281f072f47ed513d94afe7d8329c955a279a4df]:
#+begin_example
San Francisco
#+end_example

** Objects
:PROPERTIES:
:CUSTOM_ID: objects
:END:

They are key, value pair structures but they are not dictionaries.

We can access the object with "dictionary-like" notation or with the "dot" notation.

Example: =object["valueName"]= or =object.valueName=.

#+begin_src typescript
let cityObject = {
    city: "San Francisco",
    zip: 94123,
    location: [37.77, -122.43],
    inCalifornia: true
};
console.log(cityObject);
console.log("Zip: " + cityObject["zip"]);
console.log("Location: " + cityObject.location);
#+end_src

#+RESULTS[1d1710ea65ea01b4508b369f21da03eae0a237a4]:
#+begin_example
{
  city: 'San Francisco',
  zip: 94123,
  location: [ 37.77, -122.43 ],
  inCalifornia: true
}
Zip: 94123
Location: 37.77,-122.43
#+end_example

*NOTE*: We can store functions in variables, so objects can have methods by having functions as attributes.

** Example: Array
:PROPERTIES:
:CUSTOM_ID: example-array
:END:

We will start with a simple array. Then we can index it with integers. Then add elements at the begining and end of the array.

#+begin_src typescript
var letterArray = ["A", "B", "C", "D"];
console.log("--Array--\n" + letterArray);
let first = letterArray[0];
let second = letterArray[1];
console.log("Index 0 and 1", first, second);
letterArray.push("E");
letterArray.unshift("Z");
console.log("--Push and Unshift--\n" + letterArray);
var slice1 = letterArray.slice(4);
// Return the first three items of an array
console.log("--Sliced--\n" +  slice1);
// Return the second and third items of an array
var slice2 = letterArray.slice(1, 3);
console.log("--Sliced Range---\n", slice2);
// Use join() to return items of an array into a single string
var str =  letterArray.join(", ")
console.log("--Joined string--\n", str);
// A JavaScript string
var string1 = "Access element of string"
// Use indexing to access a string character
console.log(`--${string1}--\n`, string1[0], string1[1], string1[2], string1[3]);
#+end_src

#+RESULTS[0dca4c2ff60a58a07b85e9741fe9fe6a61557b7b]:
#+begin_example
--Array--
A,B,C,D
Index 0 and 1 A B
--Push and Unshift--
Z,A,B,C,D,E
--Sliced--
D,E
--Sliced Range---
 [ 'A', 'B' ]
--Joined string--
 Z, A, B, C, D, E
--Access element of string--
 A c c e
#+end_example

* Loops
:PROPERTIES:
:CUSTOM_ID: loops
:END:

Loops just as with any other language. Bonus: checking for undefined.

#+begin_src typescript
var students = ["Bob", "Alice", "Garfield", "Stitch"]
for (var i = 0; i < 5; i++){
    if (students[i] != undefined){
        console.log(i + "-" + students[i]);
    }
}
#+end_src

#+RESULTS[4b90a73581c679024f0912d82f5441c58c20a36f]:
#+begin_example
0-Bob
1-Alice
2-Garfield
3-Stitch
#+end_example

** Example: Loops
:PROPERTIES:
:CUSTOM_ID: example-loops
:END:

In this example we will loop over an array and check if the value passes a condition, then add it to a new array depending on it.

#+begin_src typescript
// Array of movie ratings
var movieScores = [
  4.4,
  3.3,
  5.9,
  8.8,
  1.2,
  5.2,
  7.4,
  7.5,
  7.2,
  9.7,
  4.2,
  6.9
];

// Starting a rating count
var sum = 0;

// Arrays to hold movie scores
var goodMovieScores = [];
var okMovieScores = [];
var badMovieScores = [];

for (var i = 0; i < movieScores.length; i++){
    let score = movieScores[i];
    sum += score;
    if (score >= 7){
        goodMovieScores.push(score);
    }
    else if (score >= 5 && score < 7){
        okMovieScores.push(score);
    }
    else {
        badMovieScores.push(score);
    }
}
var avg = sum / movieScores.length;
var nGood = goodMovieScores.length;
var nOk = okMovieScores.length;
var nBad = badMovieScores.length;
console.log("Average: " + avg);
console.log("Good movies count " + nGood);
console.log("Ok movies count " + nOk);
console.log("Bad movies count " + nBad);
#+end_src

#+RESULTS[45e163e84f8242a6c3bc1148c68a006298e9a6b3]:
#+begin_example
Average: 5.975000000000001
Good movies count 5
Ok movies count 3
Bad movies count 4
#+end_example

* Functions
:PROPERTIES:
:CUSTOM_ID: functions
:END:

We can create functions with the =function= keyword.

#+begin_src typescript
function printHello(){
    console.log("Hello there!");
}
function addition(a, b){
    return a + b;
}
function listLoop(userList){
    for (let i = 0; i < userList.length; i++){
        console.log(i);
    }
}
// Calling functions.
printHello();
// Calling functions from functions.
function doubleAddition(a, b){
    // Using the result of a function.
    var res = addition(a, b) * 2;
    return res;
}
console.log(doubleAddition(2, 3));
#+end_src

#+RESULTS[4b509f7b11437a87eb583f804fe044f8d45f8a92]:
#+begin_example
Hello there!
10
#+end_example

** Built-in Functions
:PROPERTIES:
:CUSTOM_ID: built-in-functions
:END:

We can use built-in functions from JS [fn:3]

#+begin_src typescript
var longDecimal = 112.3453410
var rounded = Math.floor(longDecimal);
console.log(rounded);
#+end_src

#+RESULTS[1e80a20244ffa7bf3d4d2c5fa5b399dd38040575]:
#+begin_example
112
#+end_example

** Example: Functions
:PROPERTIES:
:CUSTOM_ID: example-functions
:END:

We will compute the standard deviation of an array by implementing our own functions.

#+begin_src typescript
var movieScore = [4.4, 3.3, 5.9, 8.8, 1.2, 5.2, 7.4, 7.5, 7.2, 9.7, 4.2, 6.9];

function getMean(array){
    var total = 0;
    for (let i = 0; i < array.length; i++){
        total += array[i];
    }
    var meanValue = total / array.length;
    return meanValue;
}
function getVariance(array){
    var meanValue = getMean(array);
    var total = 0;
    for (var i = 0; i < array.length; i++){
        total += (array[i] - meanValue) ** 2;
    }
    var varianceValue = total / array.length;
    return varianceValue;
}
function getSTD(array){
    var varianceValue = getVariance(array);
    var STD = Math.sqrt(varianceValue);
    return STD;
}

var std = getSTD(movieScore);
console.log("Standard Deviation is: " + std);
#+end_src

#+RESULTS[b806bff56fcc5d9d05a672389633369f9104826e]:
#+begin_example
Standard Deviation is: 2.3231175174751706
#+end_example

Because Javascript doesn't include a function for =mean= or =std=, we have to implement them ourselves. However we can always import math libraries to help us deal with it. [fn:4]

* Functional Javascript
:PROPERTIES:
:CUSTOM_ID: functional-javascript
:END:

** For Each
:PROPERTIES:
:CUSTOM_ID: for-each
:END:

=forEach= requires a data structure that we want to work with and iterates over the elements within the structure.

For each loops come from the functional programming paradigm, where we mostly work with passing functions as arguments.

Each object in the data structure will have the given function applied to it. Also known as executing the callback in each of the elements.

#+begin_src typescript
var data = [1, 2, 4, 3]
var biggest = 0;
var sum = 0;

function cumulativesum(score){
  if (score > biggest){
    biggest = score;
  }
  sum += score;
}
// Using forEach
data.forEach(cumulativesum);
console.log("Cumulative Sum:", sum);
console.log("Biggest Number:", biggest);
#+end_src

#+RESULTS[424363aced28c8dc9c37a3bc13d2ee2f450e288f]:
#+begin_example
Cumulative Sum: 10
Biggest Number: 4
#+end_example

** The Arrow Function
:PROPERTIES:
:CUSTOM_ID: the-arrow-function
:END:

An arrow function is an anonymus function, the syntax comes from the =ES6= standard and it's the same as writing =function(args){}= but using the arrow for better readability =(args) => {}=.

#+begin_src typescript
let data = [0, 1, 2, 3]
data.forEach((score) => {
  console.log(score);
})
#+end_src

#+RESULTS[c597aeff694e2efaf5c28ea357091116070710b8]:
#+begin_example
0
1
2
3
#+end_example

* JSON
:PROPERTIES:
:CUSTOM_ID: json
:END:

The Javascript Object Notation is a standard for storing data as a JS object.

We can access the members of a JSON with a key or dot notation. We can also use the =Object= global type to access data from the object.

#+begin_src typescript :tangle json_test.ts
let people = {
  mom: "wilma",
  dad: "fred",
  daughter: "pebbles",
  son: "bambam"
};
let key = "mom";
console.log(people[key]);
console.log(people.son);
// Accessing properties
console.log(Object.keys(people));
console.log(Object.values(people));
console.log(Object.entries(people));
#+end_src

#+RESULTS[c9517bd1719cec552817d98713a2039293233561]:
#+begin_example
wilma
bambam
[ 'mom', 'dad', 'daughter', 'son' ]
[ 'wilma', 'fred', 'pebbles', 'bambam' ]
[
  [ 'mom', 'wilma' ],
  [ 'dad', 'fred' ],
  [ 'daughter', 'pebbles' ],
  [ 'son', 'bambam' ]
]
#+end_example

** JSON and Functions
:PROPERTIES:
:CUSTOM_ID: json-and-functions
:END:

We can combine JSON and For Each to execute an arrow function in each element of the object.

#+begin_src typescript
let character = {
  name: "homer",
  age: 42,
  simpson: true
}
Object.entries(character).forEach(([key, value]) => {
  console.log(`Key: ${key}, Value: ${value}`)
});
#+end_src

#+RESULTS[6012bca59ba231e1b0e33c36bb1e17b50d845ff1]:
#+begin_example
Key: name, Value: homer
Key: age, Value: 42
Key: simpson, Value: true
#+end_example

We used the builtin =Object= and its method =entries= to use the object as an array of key, value pairs.

** Example: Objects
:PROPERTIES:
:CUSTOM_ID: example-objects
:END:

We can create a function that counts how many times a word appears in a string by applying all the previous concepts.

#+begin_src typescript
function countWordFreq(text){
  var array = text.split(" ");
  var wordFreq = {};
  // Iterate through the array and count the occurrence of each word
  array.forEach((word) => {
    word = word.toLowerCase();
    if (word in wordFreq){
      wordFreq[word] += 1;
    }
    else {
      wordFreq[word] = 1;
    }
  });
  return wordFreq;
}
//  Call the function with the string as a parameter.
let text = "Red red green blue red bluE"
console.log(text);
console.log(countWordFreq(text));
text = "I yam what I yam and always will be what I yam"
console.log(text);
console.log(countWordFreq(text));
#+end_src

#+RESULTS[48c60ed4b9e64cf17b264af5eb7ce9af21bc5cac]:
#+begin_example
Red red green blue red bluE
{ red: 3, green: 1, blue: 2 }
I yam what I yam and always will be what I yam
{ i: 3, yam: 3, what: 2, and: 1, always: 1, will: 1, be: 1 }
#+end_example

* CSS
:PROPERTIES:
:CUSTOM_ID: css
:END:

We can style our tables, divs and elements by using Cascading Style Sheets. CSS is applied in this order =Tag->Class->ID=.

#+begin_src css :eval no
/* Tag */
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
/* Class */
.text-2u {
    color: hotpink;
}
/* ID */
#NY {
    font-size: 16px;
}
#+end_src

We can import stylesheets in the =<head>= part of the HTML document.

#+begin_src html :eval no
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
#+end_src

** Bootstrap
:PROPERTIES:
:CUSTOM_ID: bootstrap
:END:

We can include pre-made CSS styles by importing them in the header. We must use the classes provided by whichever pre-made style we choose.

We can always mix the Bootstrap CSS with our own. It is always a good idea to look at the documentation [fn:5].

* D3
:PROPERTIES:
:CUSTOM_ID: d3
:END:

Data Driven Documents [fn:6] is a Javascript library to visualize data. To use it we import it in our html header.

#+begin_src html
<script src="https://d3js.org/d3.v5.min.js"></script>
#+end_src

D3 uses Scalable Vector Graphics [fn:7] to display the data with vectors instead of bitmaps.

We can include data from external files by adding a =<script>= tag in our HTML document. This will include the objects and functions from those scripts in our document.

#+begin_src html
<head>
</head>
<body>
</body>
<script src="data.js"></script>
<script src="index_starter.js"></script>
#+end_src

** Example: D3
:PROPERTIES:
:CUSTOM_ID: example-d3
:END:

This is our base HTML document.

#+begin_src html :tangle ../resources/11-2-Student_Resources/05-Evr_D3_Table/index.html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <script src="https://d3js.org/d3.v5.min.js"></script>
</head>

<body>
  <table>
    <thead>
      <tr>
        <th>Weekday</th>
        <th>Date</th>
        <th>High</th>
        <th>Low</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
</body>
<script src="data.js"></script>
<script src="index_starter.js"></script>

</html>
#+end_src

This is our =data.js=. We will access the data array from our other javascript file.

#+begin_src typescript :results value file :file ../resources/11-2-Student_Resources/05-Evr_D3_Table/data.js :post :wrap org :exports code
const data = [
  {
    weekday: "SUN",
    date: "July 1",
    high: 76,
    low: 63
  }, {
    weekday: "MON",
    date: "July 2",
    high: 77,
    low: 63
  }, {
    weekday: "TUE",
    date: "July 3",
    high: 77,
    low: 63
  }, {
    weekday: "WED",
    date: "July 4",
    high: 81,
    low: 65
  }, {
    weekday: "THU",
    date: "July 5",
    high: 87,
    low: 68
  }, {
    weekday: "FRI",
    date: "July 6",
    high: 91,
    low: 71
  }, {
    weekday: "SAT",
    date: "July 7",
    high: 91,
    low: 72
  }
];
#+end_src

#+RESULTS[5d1281d3021a6c9ebf1d81a5168dceb56012477f]:
#+begin_org
[[file:./../resources/11-2-Student_Resources/05-Evr_D3_Table/data.js]]
#+end_org

We will append =<tr>= nodes to our table body using a script. We don't have the nodes in our HTML file by default but Javascript will add the to the DOM (which is the result of the HTMl + JS).

#+begin_src typescript :results value file :file ../resources/11-2-Student_Resources/05-Evr_D3_Table/index_starter.js :post :wrap org :exports code
let tbody = d3.select("tbody");
// Console.log the weather data from data.js
console.log(data);
// Append new rows to the table
data.forEach((weatherReport) => {
  let row = tbody.append("tr");
  Object.values(weatherReport).forEach((value) => row.append("td").text(value));
});
#+end_src

#+RESULTS[890d3b73b851b4724037bec0c4acd5c754876a8a]:
#+begin_org
[[file:./../resources/11-2-Student_Resources/05-Evr_D3_Table/index_starter.js]]
#+end_org

** Python Test
:PROPERTIES:
:CUSTOM_ID: python-test
:END:

The Javascript code ends there, but we are going to sneak in a Python test for scraping the data from the HTML document and display it here.

#+begin_src python :wrap export html
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup
from splinter import Browser
from pathlib import Path


def init_browser(url: str):
    executable_path = ChromeDriverManager().install()
    b = Browser("chrome", executable_path=executable_path, headless=True)
    b.visit(url)
    return b

parent = Path.cwd().parent
url = parent / "resources/11-2-Student_Resources/05-Evr_D3_Table/index.html"
# Absolute path using file://
browser = init_browser(f"file://{url}")
soup = BeautifulSoup(browser.html, 'html.parser')
table = soup.find("table")
browser.quit()
print(f"{table}")
#+end_src

#+RESULTS[f08cf69db3d0c08c30ef4c64a19502838950c866]:
#+begin_export html
<table>
<thead>
<tr>
<th>Weekday</th>
<th>Date</th>
<th>High</th>
<th>Low</th>
</tr>
</thead>
<tbody><tr><td>SUN</td><td>July 1</td><td>76</td><td>63</td></tr><tr><td>MON</td><td>July 2</td><td>77</td><td>63</td></tr><tr><td>TUE</td><td>July 3</td><td>77</td><td>63</td></tr><tr><td>WED</td><td>July 4</td><td>81</td><td>65</td></tr><tr><td>THU</td><td>July 5</td><td>87</td><td>68</td></tr><tr><td>FRI</td><td>July 6</td><td>91</td><td>71</td></tr><tr><td>SAT</td><td>July 7</td><td>91</td><td>72</td></tr></tbody>
</table>
#+end_export

* Events
:PROPERTIES:
:CUSTOM_ID: events
:END:

We use events to react to actions from the user. There are many events available in Javascript [fn:8], here are some of the most common:

- Click
- Change
- Keydown
- Scroll
- Pointerender

** D3 Event Listeners
:PROPERTIES:
:CUSTOM_ID: d3-event-listeners
:END:

An event listener looks like this: =Object(Event, Handler);=.

#+begin_src typescript :eval no
// Object( Event,  Handler    );
button.on("click", handleClick);
#+end_src

If we have a button in the html.
#+begin_src html
<button id="click-me">Click me!</button>
<input id="input-field"></input>
#+end_src

We must create a reference to the button with D3.

#+begin_src typescript :eval no
// Element
var button = d3.select("#click-me");
var inputField = d3.select("#input-field");
// Handler
function handleClick(){
  console.log("Button clicked!");
  console.log(d3.event.target);
}
// Object( Event,  Handler )
button.on("click", handleClick);
#+end_src

An event is an object that contains all the data from the interaction. The target property is a node in the DOM. So we can grab the target to get more information. This way we can know in which element the event happened.

In this case our target is the button with the id ="click-me"=.

** Adding HTML
:PROPERTIES:
:CUSTOM_ID: adding-html
:END:

We can add HTML elements to the DOM dynamically using events.

#+begin_src typescript :eval no
function handleClick(){
  let text = "Hello D3!";
  // Selecting a class and adding a header to it.
  d3.select(".giphy-me").html(`<h1>${text}</h1>`);
}
#+end_src

** Example: Events
:PROPERTIES:
:CUSTOM_ID: example-events
:END:

We need to create a script that will react to user input and create a reversed string in a new HTML element.

#+begin_src typescript :post :wrap org :results value file :file ../resources/11-2-Student_Resources/07-Stu_onChange/js/app_starter.js :exports code
var output = d3.select(".output"); // class
var inputField = d3.select("#my-input"); // id
var ul = d3.select("#my-ul")
// Functions
function reverseString(str) {
  return str.split("").reverse().join("");
}
function handleChange(event) {
  // grab the value of the input field
  // Using global d3 event instead of local one from this function
  let value = d3.event.target.value;
  // clear the existing output
  output.html("");
  // reverse the input string
  let reversed = reverseString(value);
  // Set the output text to the reversed input string
  output.text(reversed);
}
// Event
inputField.on("input", handleChange);
#+end_src

#+RESULTS[538b31144c969c7e3de827de817cb1412c454645]:
#+begin_org
[[file:./../resources/11-2-Student_Resources/07-Stu_onChange/js/app_starter.js]]
#+end_org

We can load the script in this document as long as we are importing =D3=, it will work for the HTML version but not for Markdown.

#+begin_export html
<div id="my-div">
    <label id="my-label" for="text">Input Text: </label>
    <input id="my-input" type="text" name="text-input", value="tacocat">
</div>
<h1 class="output"></h1>
<ul id="my-ul"></ul>
<script src="../resources/11-2-Student_Resources/07-Stu_onChange/js/app_starter.js"></script>
#+end_export


* Footnotes
:PROPERTIES:
:CUSTOM_ID: footnotes
:END:
[fn:8]https://developer.mozilla.org/en-US/docs/Web/Events

[fn:7]https://en.wikipedia.org/wiki/Scalable_Vector_Graphics

[fn:6]https://observablehq.com/@d3/gallery

[fn:5]https://getbootstrap.com/docs/5.2/getting-started/introduction/

[fn:4]https://mathjs.org/docs/reference/functions/mean.html

[fn:3]https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects

[fn:2]https://blog.openreplay.com/javascript-type-conversions-explained

[fn:1]https://developer.mozilla.org/en-US/docs/Web/JavaScript
