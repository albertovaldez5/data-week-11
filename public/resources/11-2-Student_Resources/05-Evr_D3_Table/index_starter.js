var tbody = d3.select("tbody");
// Console.log the weather data from data.js
console.log(data);
// Append new rows to the table
data.forEach(function (weatherReport) {
    var row = tbody.append("tr");
    Object.values(weatherReport).forEach(function (value) { return row.append("td").text(value); });
});
