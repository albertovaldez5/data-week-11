// Array of movie ratings
var movieScores = [
  4.4,
  3.3,
  5.9,
  8.8,
  1.2,
  5.2,
  7.4,
  7.5,
  7.2,
  9.7,
  4.2,
  6.9
];

// Starting a rating count
var sum = 0;

// Arrays to hold movie scores
var goodMovieScores = [];
var okMovieScores = [];
var badMovieScores = [];

for (var i = 0; i < movieScores.length; i++){
    let score = movieScores[i];
    sum += score;
    if (score >= 7){
        goodMovieScores.push(score);
    }
    else if (score >= 5 && score < 7){
        okMovieScores.push(score);
    }
    else {
        badMovieScores.push(score);
    }
}
var avg = sum / movieScores.length;
var nGood = goodMovieScores.length;
var nOk = okMovieScores.length;
var nBad = badMovieScores.length;
console.log("Average: " + avg);
console.log("Good movies count " + nGood);
console.log("Ok movies count " + nOk);
console.log("Bad movies count " + nBad);
