var movieScore = [4.4, 3.3, 5.9, 8.8, 1.2, 5.2, 7.4, 7.5, 7.2, 9.7, 4.2, 6.9];

function getMean(array){
    var total = 0;
    for (let i = 0; i < array.length; i++){
        total += array[i];
    }
    var meanValue = total / array.length;
    return meanValue;
}
function getVariance(array){
    var meanValue = getMean(array);
    var total = 0;
    for (var i = 0; i < array.length; i++){
        total += (array[i] - meanValue) ** 2;
    }
    var varianceValue = total / array.length;
    return varianceValue;
}
function getSTD(array){
    var varianceValue = getVariance(array);
    var STD = Math.sqrt(varianceValue);
    return STD;
}

var std = getSTD(movieScore);
console.log("Standard Deviation is: " + std);
