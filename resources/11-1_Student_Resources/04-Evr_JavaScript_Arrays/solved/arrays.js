// Here, start with a blank array
var letterArray = ["A", "B", "C", "D"];
console.log("--Array--\n" + letterArray);
// Use indexing to access an array item
let first = letterArray[0];
let second = letterArray[1];
console.log("Index 0 and 1", first, second);
// Use push() to append an item to an array, unshift() adds an element at the begining.
letterArray.push("E");
letterArray.unshift("Z");
console.log("--Push and Unshift--\n" + letterArray);
// Use slice() to return selected items of an array
var slice1 = letterArray.slice(4);
// Return the first three items of an array
console.log("--Sliced--\n" +  slice1);
// Return the second and third items of an array
var slice2 = letterArray.slice(1, 3);
console.log("--Sliced Range---\n", slice2);
// Use join() to return items of an array into a single string
var str =  letterArray.join(", ")
console.log("--Joined string--\n", str);
// A JavaScript string
var string1 = "Access element of string"
// Use indexing to access a string character
console.log(`--${string1}--\n`, string1[0], string1[1], string1[2], string1[3]);
// Split a string into an array of substrings
// Here, split the string where spaces are found
