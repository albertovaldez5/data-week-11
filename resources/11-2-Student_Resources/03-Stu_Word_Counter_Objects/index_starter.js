// Initialize the function
function countWordFreq(text){
  // Convert string to an array of words
  var array = text.split(" ");
  // An object to hold word frequency
  var wordFreq = {};
  // Iterate through the array and count the occurrence of each word
  array.forEach((word) => {
    word = word.toLowerCase();
    if (word in wordFreq){
      wordFreq[word] += 1;
    }
    else {
      wordFreq[word] = 1;
    }
  });
  return wordFreq;
}
//  Call the function with the string as a parameter.
let text = "Red red green blue red bluE"
console.log(text);
console.log(countWordFreq(text));
text = "I yam what I yam and always will be what I yam"
console.log(text);
console.log(countWordFreq(text));
