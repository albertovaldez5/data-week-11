var output = d3.select(".output"); // class
var inputField = d3.select("#my-input"); // id
var ul = d3.select("#my-ul");
// Functions
function reverseString(str) {
    return str.split("").reverse().join("");
}
function handleChange(event) {
    // grab the value of the input field
    // Using global d3 event instead of local one from this function
    var value = d3.event.target.value;
    // clear the existing output
    output.html("");
    // reverse the input string
    var reversed = reverseString(value);
    // Set the output text to the reversed input string
    output.text(reversed);
}
// Event
inputField.on("input", handleChange);
