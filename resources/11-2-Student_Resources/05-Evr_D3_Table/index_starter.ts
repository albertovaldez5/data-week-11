let tbody = d3.select("tbody");
// Console.log the weather data from data.js
console.log(data);
// Append new rows to the table
data.forEach((weatherReport) => {
  let row = tbody.append("tr");
  Object.values(weatherReport).forEach((value) => row.append("td").text(value));
});
